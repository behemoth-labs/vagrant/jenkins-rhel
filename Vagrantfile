# -*- mode: ruby -*-
# vi: set ft=ruby :

# Variables and secrets
require 'yaml'
params = YAML.load_file 'variables.yaml'
secrets = YAML.load_file 'secrets.yaml'

servers=[
    {
        # Jenkins
        :hostname => params['jenkins']['hostname'],
        :ip1 => params['jenkins']['ip1'],
        :box => params['global']['box_name'],
        :box_json => params['global']['box_json'],
        :ram => params['jenkins']['memory'],
        :cpu => params['jenkins']['cores'],
        :disk_size => params['jenkins']['root_disk'],
        :additional_disks => params['jenkins']['additional_disks'],
        :role => params['jenkins']['role']
    }
]

Vagrant.configure("2") do |config|
    # Resize ens5's mtu to '1392'.
    # This is an internal issue due to WireGuard VPN.
    config.vm.provision "shell",
        run: "always",
        inline: "sudo ip link set dev ens5 mtu 1392"

    # Have to disable 'rsync', as it tries to install packages
    # and fail before the MTU was fixed for my local issue.
    config.nfs.verify_installed = false
    config.vm.synced_folder './sync', '/vagrant', type: 'rsync', disabled: true

    # Don't inject pub key
    config.ssh.insert_key = false
    config.ssh.username = "vagrant"
    config.ssh.password = 'vagrant'

    # Loop creating VMs
    servers.each do |machine|
        config.vm.define machine[:hostname] do |node|
            config.vm.box = machine[:box]
            config.vm.box_url = [ machine[:box_json ] ]
            node.vm.hostname = machine[:hostname]

            # Private
            node.vm.network :private_network,
                :ip => machine[:ip1]

            node.vm.provider :libvirt do |lv|
                lv.storage_pool_name = params["global"]["pool_name"]
                lv.machine_virtual_size = machine[:disk_size]
                lv.qemu_use_session = false
                lv.cpus = machine[:cpu]
                lv.driver = "kvm"
                lv.memory = machine[:ram]
                lv.nic_model_type = "virtio"
                lv.nested = true
            end

            node.vm.provision "shell", path: "scripts/01_init.sh", privileged: true, run: "once", env: {
                "RHEL_SUB_PASS" => secrets['global']['rhel_password'],
                "RHEL_SUB_USER" => secrets['global']['rhel_username'],
                "NTP_SERVER"    => params['global']['ntp_server']
            }
            node.vm.provision "shell", path: "scripts/02_disable.sh", privileged: true, run: "once"
            node.vm.provision "shell", path: "scripts/03_repository.sh", privileged: true, run: "once"
            node.vm.provision "shell", path: "scripts/04_update.sh", privileged: true, run: "once"
            node.vm.provision "shell", path: "scripts/05_install.sh", privileged: true, run: "once"
            node.vm.provision "file", source: "files/confs", destination: "/tmp/files/confs"
            node.vm.provision "shell", path: "scripts/06_configure.sh", privileged: true, run: "once", env: {
                "JENKINS_PORT" => params['jenkins']['jenkins_port'],
                "JENKINS_USER" => params['jenkins']['jenkins_user'],
                "JENKINS_PASS" => params['jenkins']['jenkins_pass']
            }
            node.vm.provision "shell", path: "scripts/07_start.sh", privileged: true, run: "once"
            node.vm.provision "shell", path: "scripts/08_plugins.sh", privileged: true, run: "once", env: {
                "JENKINS_IP"   => params['jenkins']['ip1'],
                "JENKINS_PORT" => params['jenkins']['jenkins_port'],
                "JENKINS_USER" => params['jenkins']['jenkins_user'],
                "JENKINS_PASS" => params['jenkins']['jenkins_pass']
            }
            node.vm.provision "shell", path: "scripts/09_details.sh", privileged: false, run: "always", env: {
                "JENKINS_IP"   => params['jenkins']['ip1'],
                "JENKINS_PORT" => params['jenkins']['jenkins_port'],
                "JENKINS_USER" => params['jenkins']['jenkins_user'],
                "JENKINS_PASS" => params['jenkins']['jenkins_pass']
            }
        end
    end

    config.trigger.before :destroy do |trigger|
        trigger.name = "Making sure to unregister from RHEL"
        trigger.run_remote = { inline: "sudo subscription-manager unregister" }
        trigger.on_error = :halt
    end
end
