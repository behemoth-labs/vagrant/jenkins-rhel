# Vagrant, Jenkins Installer (for Red Hat Enterprise Linux)

A vagrant project to install Jenkins with Plugins in a secured manner. If you wish, you can take all the files under the `files/` and `scripts/` directories and run them locally to perform a local install of Jenkins without Vagrant.

Currently this is an 'online' installation (require Internet connection), in the future I will add an 'offline' installation.

## Requirements

- Vagrant (2.3.4)
- Libvirt, KVM and QEMU.
- Storage => 20GB.
- Memory => 4GB.
- You own, pre-made Red Hat 8.8 Box.
- Internet Access.
- Redhat Subscription.

## Box

This lab is using a custom Box, based on Red Hat Enterprise Linux 8.8. The json file should point to the location **you** have the box file at, and have the correct hash value, for example:

```
{
    "name": "rhel8_8-10",
    "description": "Red Hat Enterprise Linux 8.8",
    "versions": [{
        "version": "8.8-10",
        "providers": [{
            "name": "libvirt",
            "url": "file:///home/itzhak/Downloads/BOXes/rhel8.8/rhel8.8-10.box",
            "checksum": "2c6c6f7cdad0fae06d36b616e440ba20f505f1272dcfc89aaabbc996aece13ab",
            "checksum_type": "sha256"
        }]
    }]
} 

```

## Variables & Secrets

You will need a `variables.yaml` file which hold the variables used when creating the VM, while these variables aren't considered secret, they may differ from place to place - the default ones may not work for you. Due to this fact, this file isn't included, and you will need to create it for yourself.

`variables.yaml`
```
---
global:
  box_name: "rhel8_8-10"
  box_json: "rhel8_8-10.json"
  pool_name: "defaults"
  ntp_server: '192.168.30.50'

jenkins:
  role: "jenkins"
  hostname: "jenkins"
  ip1: "192.168.30.50"
  memory: 12000
  cores: 4
  root_disk: 40
  jenkins_port: 8080
  jenkins_user: "test"
  jenkins_pass: "test"
---
```

Additionally, you will need a secrets file called `secrets.yaml`, even if you don't planned to use it, it is required, so just make a template. This file holds the credentials for Red Hat Access and other information considered a secret.

`secrets.yaml`
```
---
global:
  rhel_username: "<USERNAME>"
  rhel_password: "<PASSWORD>"
---
```

## Scripts & Files

The lab creation is based on scripts under the `scripts` directory and files under the `files` directory. The files you will need to collect/create yourself, follow the scripts and place the files in the corresponding path under files.

### Plugins

`files/confs/plugins.txt` include a basic list of plugins to install, modify it with according to your requirements.

### Bootstrap

If you wish to run this installation somewhere else (not as part of this Lab), create a `tar.gz` file of `files/` and `scripts/`,

```
tar czf jenkins.tar.gz files/ scripts/
```

Once you have this file on the destination server, run `00_bootstrap.sh`, edit `/tmp/bootstrap.src`, and then continue running each on of the files from `01_*.sh` to `09_*.sh` going up (make sure to run the scripts by order, if you have several scripts starting with the same number, to order of running them doesn't matter - but does for everything else).


## Running

Don't forget, this lab is based on a custom box! Without this requirements, the lab will not work.

```
vagrant validate
vagrant up
```

## Shutdown

When using `vagrant destroy`, the script is using a `config.trigger.before :destroy` to unsubsribe from Redhat. In some cases, when the provisioning failed, or when the box wasn't destroyed successfully, the subscription will remain and you will be required to manually remove it and try again.
