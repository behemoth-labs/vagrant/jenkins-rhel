#!/bin/bash
#
# Printing details

# For direct running (not via Vagrant)
if [ -f '/tmp/bootstrap.src' ]; then
  source /tmp/bootstrap.src
fi

cat <<EOF -
URL:        http://${JENKINS_IP}:${JENKINS_PORT}/
Username:   ${JENKINS_USER}
Password:   ${JENKINS_PASS}
EOF
