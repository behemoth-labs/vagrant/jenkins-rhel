#!/bin/bash
#
# Subscribing to Red Hat service (for updates).
# Growing the disk to the maximum size.
#
# This script requires root access.

# For direct running (not via Vagrant)
if [ -f '/tmp/bootstrap.src' ]; then
    source /tmp/bootstrap.src
fi

# echo 'Subscribing with RHEL...'
if [ -n "${RHEL_SUB_USER}" ] && [ -n "${RHEL_SUB_PASS}" ]; then
    subscription-manager register --username ${RHEL_SUB_USER} --password ${RHEL_SUB_PASS} \
        --auto-attach
    sleep 5 # letting subscription to propergate to registration servers
fi

# Installing growpart
yum install -y cloud-utils-growpart

# Update disk size (grow)
growpart /dev/vda 2
lvextend -l +100%FREE /dev/mapper/rhel-root
xfs_growfs /dev/mapper/rhel-root
df -h

# NTP
if [ -n "${NTP_SERVER}" ]; then
    # Only running if variables exists with a value in it
    yum install -y chrony
    sed -i "s/^pool .*/pool ${NTP_SERVER}/g" /etc/chrony.conf
    systemctl restart chronyd.service
    systemctl enable chronyd.service
fi
