#!/bin/bash
#
# Installing plugins defined in a file.

# For direct running (not via Vagrant)
if [ -f '/tmp/bootstrap.src' ]; then
  source /tmp/bootstrap.src
fi

# Installing plugins
if [ -f '/tmp/files/confs/plugins.txt' ]; then
    # Waiting for Jenkins to sattle down
    sleep 60

    # Downloading CLI tool
    curl http://${JENKINS_IP}:${JENKINS_PORT}/jnlpJars/jenkins-cli.jar -o /tmp/jenkins-cli.jar

    # Installing plugins
    xargs -I {} java -jar /tmp/jenkins-cli.jar -s http://${JENKINS_IP}:${JENKINS_PORT}/ -auth ${JENKINS_USER}:${JENKINS_PASS} install-plugin "{}" < /tmp/files/confs/plugins.txt

    # Safe restarting Jenkins
    java -jar /tmp/jenkins-cli.jar -s http://${JENKINS_IP}:${JENKINS_PORT}/ -auth ${JENKINS_USER}:${JENKINS_PASS} safe-restart
fi
