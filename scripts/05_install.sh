#!/bin/bash
#
# Installing Jenkins and it's dependencies.
#
# This script requires root access.

yum install -y fontconfig java-17-openjdk
yum install -y jenkins
