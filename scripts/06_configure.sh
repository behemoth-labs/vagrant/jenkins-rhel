#!/bin/bash
#
# Configuring Jenkins before first run.
#
# This script requires root access.

# For direct running (not via Vagrant)
if [ -f '/tmp/bootstrap.src' ]; then
  source /tmp/bootstrap.src
fi

# Creating a secured setup (username and password)
#mkdir -p /usr/share/jenkins/ref/init.groovy.d
#mv -fv /tmp/files/confs/security.groovy /usr/share/jenkins/ref/init.groovy.d/security.groovy
echo "2.0" > /var/lib/jenkins/jenkins.install.UpgradeWizard.state
mkdir /var/lib/jenkins/init.groovy.d
mv -fv /tmp/files/confs/security.groovy /var/lib/jenkins/init.groovy.d/basic-security.groovy
sed -i "s/###JENKINS_USER###/${JENKINS_USER}/g" /var/lib/jenkins/init.groovy.d/basic-security.groovy
sed -i "s/###JENKINS_PASS###/${JENKINS_PASS}/g" /var/lib/jenkins/init.groovy.d/basic-security.groovy

# Updating service unit
mv -fv /tmp/files/confs/jenkins.service /usr/lib/systemd/system/jenkins.service
sed -i "s/###JENKINS_PORT###/${JENKINS_PORT}/g" /usr/lib/systemd/system/jenkins.service
chown root: /usr/lib/systemd/system/jenkins.service
chmod 0644 /usr/lib/systemd/system/jenkins.service

# Reloading service files
systemctl daemon-reload
