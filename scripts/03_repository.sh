#!/bin/bash
#
# Configuring Jenkins' repository and key.
#
# Guides:
#  - https://www.redhat.com/sysadmin/install-jenkins-rhel8
#  - https://pkg.jenkins.io/redhat-stable/
#
# This script requires root access.

curl https://pkg.jenkins.io/redhat-stable/jenkins.repo -o /etc/yum.repos.d/jenkins.repo
rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io-2023.key
