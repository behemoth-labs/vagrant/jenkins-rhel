#!/bin/bash
#
# Enabling and starting Jenkins.
#
# This script requires root access.

systemctl enable --now jenkins
