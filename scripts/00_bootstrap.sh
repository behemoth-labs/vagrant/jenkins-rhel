#!/bin/bash
#
# If you've extracted the files manually, run this script first
# to place them in their expected place.

mv "$(dirname $0)/../files" /tmp

cat <<EOF > /tmp/bootstrap.src
#!/bin/bash
#
# Populate the following values.

# Red Hat subscription credentials
export RHEL_SUB_USER="username"
export RHEL_SUB_PASS="password"

# NTP server
export NTP_SERVER="127.0.0.1"

# Instance IP and port
export JENKINS_IP="127.0.0.1"
export JENKINS_PORT=8080

# Jenkins credentials
export JENKINS_USER="example_user"
export JENKINS_PASS="example_pass"
EOF

echo "Please edit '/tmp/bootstrap.src' file before continuing to the rest of the scripts."
echo "Run the scripts as 'root'."
